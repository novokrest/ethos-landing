# Ethos Landing Code Challenge

## Author

Konstantin Novokreshchenov  
email: novokrest013@gmail.com  

## How To

Clone this repository and run .NET console app  

```  
git clone https://bitbucket.org/novokrest/ethos-landing.git  
cd ethos-landing/challenge  
dotnet run  
```  
