namespace challenge
{
    interface ILoanDataParamInitializer
    {
        bool MatchParam(string paramName);

        void InitializeLoanDataParameter(string rawValue, LoanData.Builder loanDataBuilder);
    }
}