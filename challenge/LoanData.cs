using System;
using System.Numerics;

namespace challenge
{
    class LoanData
    {
        private LoanData(decimal initialAmount,
                         decimal downPaymentAmount,
                         double interestRate,
                         uint termYears)
        {
            InitialAmount = initialAmount;
            DownPaymentAmount = downPaymentAmount;
            InterestRate = interestRate;
            TermYears = termYears;   
        }

        public decimal InitialAmount { get; }

        public decimal DownPaymentAmount { get; }
        
        public double InterestRate { get; }

        public uint TermYears { get; }

        public static Builder builder()
        {
            return new Builder();
        }

        public class Builder
        {
            private decimal _initialAmount;
            private decimal _downPaymentAmount;
            private double _interestRate;
            private uint _termYears;
            
            public Builder withInitialAmount(decimal initialAmount)
            {
                this._initialAmount = initialAmount;
                return this;
            }

            public Builder withDownPaymentAmount(decimal downPaymentAmount)
            {
                this._downPaymentAmount = downPaymentAmount;
                return this;
            }

            public Builder withInterestRate(double interestRate)
            {
                this._interestRate = interestRate;
                return this;
            }

            public Builder withTermYears(uint termYears)
            {
                this._termYears = termYears;
                return this;
            }

            public LoanData build()
            {
                return new LoanData
                (
                    _initialAmount,
                    _downPaymentAmount, 
                    _interestRate, 
                    _termYears
                );
            }
        }
    }
}