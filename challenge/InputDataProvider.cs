using System;
using System.Collections.Generic;

namespace challenge
{
    interface IInputDataProvider
    {
        IEnumerable<string> GetInputData();
    }

    class ConsoleInputDataProvider : IInputDataProvider
    {
        public IEnumerable<string> GetInputData()
        {
            bool isLastLineNotEmpty;
            do
            {
                string lastReadLine = Console.ReadLine()?.Trim();
                isLastLineNotEmpty = !string.IsNullOrEmpty(lastReadLine);
                if (isLastLineNotEmpty)
                {
                    yield return lastReadLine;
                }
            } while (isLastLineNotEmpty);
        }
    }
}