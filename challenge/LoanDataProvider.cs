using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace challenge
{
    class LoanDataProvider
    {
        private static readonly Regex InputDataFormatRegex = new Regex("(\\w+)\\s*:\\s*(\\S+)");
        private static IReadOnlyList<ILoanDataParamInitializer> Parsers = new List<ILoanDataParamInitializer>
        {
            new LoanDataParamParser<decimal>
            (
                "amount", 
                new DecimalConverter(), 
                (amount, loanDataBuilder) => loanDataBuilder.withInitialAmount(amount)
            ),

            new LoanDataParamParser<decimal>
            (
                "downpayment", 
                new DecimalConverter(), 
                (downPayment, loanDataBuilder) => loanDataBuilder.withDownPaymentAmount(downPayment)
            ),
            
            new LoanDataParamParser<double>
            (
                "interest", 
                new DoubleOrPercentConverter(), 
                (interestRate, loanDataBuilder) => loanDataBuilder.withInterestRate(interestRate)
            ),
            
            new LoanDataParamParser<uint>(
                "term", 
                new UIntConverter(), 
                (termYears, loanDataBuilder) => loanDataBuilder.withTermYears(termYears)
            )
        };

        private readonly IInputDataProvider _dataProvider;

        public LoanDataProvider(IInputDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public LoanData GetLoanData()
        {
            LoanData.Builder loanDataBuilder = LoanData.builder();
            
            foreach (var inputData in _dataProvider.GetInputData())
            {
                ParseParam(inputData, loanDataBuilder);
            }

            return loanDataBuilder.build();
        }

        private void ParseParam(string inputData, LoanData.Builder loanDataBuilder)
        {
            Match match = InputDataFormatRegex.Match(inputData);
            if (match.Success)
            {
                string normalizedParamName = match.Groups[1].Value.ToLower();
                string rawParamValue = match.Groups[2].Value;
                InitializeLoanDataParameter(normalizedParamName, rawParamValue, loanDataBuilder);
            }
            else
            {
                throw new Exception($"Incorrect input data: '{inputData}'");
            }
        }

        private void InitializeLoanDataParameter(string normalizedParamName, 
                                                 string rawParamValue, 
                                                 LoanData.Builder loanDataBuilder)
        {
            var paramParser = Parsers.FirstOrDefault(parser => parser.MatchParam(normalizedParamName));
            if (paramParser == null)
            {
                throw new Exception($"Unexpected data parameter: '{normalizedParamName}'");
            }
            paramParser.InitializeLoanDataParameter(rawParamValue, loanDataBuilder);
        }

        private class LoanDataParamParser<TParamValue> : ILoanDataParamInitializer
        {
            private readonly string _paramName;
            private readonly IRawValueConverter<TParamValue> _valueConverter;
            private readonly Action<TParamValue, LoanData.Builder> _paramSetter;

            public LoanDataParamParser(string paramName,
                                       IRawValueConverter<TParamValue> valueConverter,
                                       Action<TParamValue, LoanData.Builder> paramSetter)
            {
                _paramName = paramName;
                _valueConverter = valueConverter;
                _paramSetter = paramSetter;
            }

            public bool MatchParam(string paramName)
            {
                return Object.Equals(_paramName, paramName);
            }

            public void InitializeLoanDataParameter(string rawValue, LoanData.Builder loanDataBuilder)
            {
                TParamValue value;
                if (_valueConverter.Convert(rawValue, out value))
                {
                    _paramSetter(value, loanDataBuilder);
                }
                else
                {
                    throw new Exception($"Incorrect parameter's value: parameter={_paramName}, value={rawValue}");
                }
            }
        }
    }
}