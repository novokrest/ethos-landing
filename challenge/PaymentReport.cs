using System;
using Newtonsoft.Json;

namespace challenge
{
    class PaymentReport
    {
        private PaymentReport(decimal monthlyPayment, 
                              decimal totalInterest, 
                              decimal totalPayment)
        {
            MonthlyPayment = monthlyPayment;
            TotalInterest = totalInterest;
            TotalPayment = totalPayment;
        }

        [JsonProperty("monthly payment")]
        public decimal MonthlyPayment { get; }

        [JsonProperty("total interest")]
        public decimal TotalInterest { get; }

        [JsonProperty("total payment")]
        public decimal TotalPayment { get; }

        public static Builder builder()
        {
            return new Builder();
        }

        public class Builder
        {
            private decimal _monthlyPayment;
            private decimal _totalInterest;
            private decimal _totalPayment;

            public Builder withMonthlyPayment(decimal monthlyPayment)
            {
                this._monthlyPayment = monthlyPayment;
                return this;
            }

            public Builder withTotalInterest(decimal totalInterest)
            {
                this._totalInterest = totalInterest;
                return this;
            }

            public Builder withTotalPayment(decimal totalPayment)
            {
                this._totalPayment = totalPayment;
                return this;
            }

            public PaymentReport build()
            {
                return new PaymentReport
                (
                    _monthlyPayment,
                    _totalInterest, 
                    _totalPayment
                );
            }
        }
    }

    static class PaymentReportExtension
    {
        public static void ToConsole(this PaymentReport report)
        {
            new ConsolePrettyPaymentReportWriter().WriteReport(report);
        }

        public static PaymentReport Round(this PaymentReport report, int decimals)
        {
            return PaymentReport.builder()
                .withMonthlyPayment(Math.Round(report.MonthlyPayment, decimals))
                .withTotalInterest(Math.Round(report.TotalInterest, decimals))
                .withTotalPayment(Math.Round(report.TotalPayment, decimals))
                .build();
        }
    }
}