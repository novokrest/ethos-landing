namespace challenge
{
    interface IRawValueConverter<TValue>
    {
        bool Convert(string rawValue, out TValue result);
    }

    class DecimalConverter : IRawValueConverter<decimal>
    {
        public bool Convert(string rawValue, out decimal result)
        {
            return decimal.TryParse(rawValue, out result);
        }
    }

    class UIntConverter : IRawValueConverter<uint>
    {
        public bool Convert(string rawValue, out uint result)
        {
            return uint.TryParse(rawValue, out result);
        }
    }

    class DoubleOrPercentConverter : IRawValueConverter<double>
    {
        private const char PERCENT = '%';

        public bool Convert(string rawValue, out double result)
        {
            if (string.IsNullOrEmpty(rawValue))
            {
                result = 0D;
                return false;
            }

            if (rawValue.EndsWith(PERCENT))
            {
                double percents;
                if (double.TryParse(rawValue.Substring(0, rawValue.Length - 1), out percents))
                {
                    result = percents / 100;
                    return true;
                }

                result = 0D;
                return false;
            }

            return double.TryParse(rawValue, out result);
        }
    }
}