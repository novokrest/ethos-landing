namespace challenge
{
    class LoanPaymentCalculatorFactory
    {
        public static LoanPaymentCalculator CreateCalculator()
        {
            return new LoanPaymentCalculator(new LoanDataProvider(new ConsoleInputDataProvider()));
        }
    }
}