using System;
using System.Numerics;

namespace challenge
{
    class LoanPaymentCalculator
    {
        private const ushort MONTHS_PER_YEAR = 12;

        private readonly LoanDataProvider _loanDataProvider;

        public LoanPaymentCalculator(LoanDataProvider loanDataProvider)
        {
            _loanDataProvider = loanDataProvider;
        }

        public PaymentReport CalculatePayments()
        {
            return Calculate(_loanDataProvider.GetLoanData());
        }

        private PaymentReport Calculate(LoanData loanData)
        {
            decimal loanAmount = loanData.InitialAmount - loanData.DownPaymentAmount;
            double monthInterestRate = loanData.InterestRate / MONTHS_PER_YEAR;
            uint monthsCount = loanData.TermYears * MONTHS_PER_YEAR;

            decimal fixedRatePayment = CalculateFixedRatePayment(loanAmount, monthInterestRate, monthsCount);
            decimal totalPayment = fixedRatePayment * monthsCount;
            decimal totalInterest = totalPayment - loanAmount;

            return PaymentReport.builder()
                    .withMonthlyPayment(fixedRatePayment)
                    .withTotalPayment(totalPayment)
                    .withTotalInterest(totalInterest)
                    .build();
        }

        private decimal CalculateFixedRatePayment(decimal loanAmount, double interestRatePerPeriod, uint periodsCount)
        {
            double factor = interestRatePerPeriod / (1 - Math.Pow(1 + interestRatePerPeriod, -periodsCount));
            return (decimal) factor * loanAmount;
        }
    }
}