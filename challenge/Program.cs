﻿using System;

namespace challenge
{
    class Program
    {
        private const int SUCCESS_EXIT_CODE = 0;
        private const int ERROR_EXIT_CODE = 1; 

        public static int Main(string[] args)
        {
            if (args.Length > 0) 
            {
                Console.WriteLine("Too much arguments! Use: dotnet run");
                return ERROR_EXIT_CODE;
            }

            return CalculatePayments();
        }

        private static int CalculatePayments()
        {
            try
            {
                PaymentReport paymentReport = LoanPaymentCalculatorFactory.CreateCalculator().CalculatePayments();
                paymentReport.ToConsole();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to calculate payments: error='{0}'", ex.Message);
                Console.WriteLine(ex.StackTrace);
                return ERROR_EXIT_CODE;
            }
            
            return SUCCESS_EXIT_CODE;
        }
    }
}
