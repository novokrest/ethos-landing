using System;
using Newtonsoft.Json;

namespace challenge
{
    interface IPaymentReportWriter
    {
        void WriteReport(PaymentReport report);
    }

    class ConsolePrettyPaymentReportWriter : IPaymentReportWriter
    {
        private const int VALUE_DECIMALS = 2;

        public void WriteReport(PaymentReport report)
        {
            string prettyJson = JsonConvert.SerializeObject(report.Round(VALUE_DECIMALS), Formatting.Indented);
            Console.WriteLine(prettyJson);
        }
    }
}